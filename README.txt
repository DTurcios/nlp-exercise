README.txt
exercise.java Version 1.0
11/18/2018

Programmer: Dany Turcios
=============
General Notes
=============
-Programmed in Ecplise on Windows 10
-Java 1.8.0_131

===============
Compiling Notes
===============

-Compile java program "javac ProgamName"
-Execute java program "java ProgramName arguments"

-Add up to three arguments
	First argument is Document file name
	Second argument is lexicon file name
	Optional third argument is tag file name
		Default is set to be first argument with tags
		
MAKE SURE DOCUMENT AND LEXICON FILES ARE IN SAME DIRECTORY
OTHERWISE SPECIFY FILE PATH OF FILES.

==============================================================
Contact Information
===================
Email: danyxavierturcios@gmail.com