import java.io.*;

import java.util.StringTokenizer;

public class exercise {
	//Function to check if word is in lexicon document
	public static int wordCheck(String string1, String matrix[][]) {
		int index = -1;	
		for(int i = 0; i <= 599; i++) {
			if(matrix[0][i].equals(string1.toUpperCase())) {
				index = i;
				break;
			}
		}
		return index;
		
	}
	
	//Main program
	public static void main(String[] args) {
	   //Create file names
	   String document = args[0];
	   String filenameForTag = args[0]+".tag";//set as default
	   String lexicon = args[1];

	   //Check for arguments
	   if(args.length < 2) {
		   System.out.println("Error: Not enough arguments given");
	   }
	   else if(args.length == 3) {
		   filenameForTag = args[2] + ".tag";//if third argument exists replace filename
	   }
	   
	   //This is used to hold the lexicon tags and codes
	   String[][] matrix = new String[2][600];
	   
	   //Reading "lexicon" file
	   String line1 = null;
	   
	   /* Here I am reading the Lexicon file into a 2-D string array.
	    * Since I know the amount of lines is 600 in the document,
	    * I created the array to be [2][600].
	    */
	   try {
		   //Reads text files
		   FileReader fileReader = new FileReader(lexicon);
		   //Wrap FileReader in BufferReader
		   BufferedReader bufferedReader = new BufferedReader(fileReader);
		   int currentline = 0;
		   //Read every line until Null
		   while((line1 = bufferedReader.readLine()) != null) {
			   StringTokenizer st1 = new StringTokenizer(line1);
			   
			   //For lexicon file there are only two tokens per line
			   for (int i = 1; st1.hasMoreTokens(); i++) {
				   //Assign line to a 2 by n array
				   if(i == 1){
					   matrix[0][currentline] = st1.nextToken();//assign code
				   }
				   else{
					   matrix[1][currentline] = st1.nextToken();//assign tag
				   }
			   }
			   currentline += 1;
		   }
		   bufferedReader.close();
	   }
	   catch(FileNotFoundException ex){
		   System.out.println("File: "+lexicon+" not found");   
	   }
	   catch(IOException ex) {
		   System.out.println("Error opening "+lexicon);  
	   }

	   //Reading "document" file
	   String line = null;
	   
	   /* Here I am reading "document" line by line.
	    * I will be reading each line and comparing each "token" within the line
	    * to the string array of lexicons.
	    * 
	    * Then output the tag after the token in a new document.
	    */
	   
	   try {
		   //Reads text files
		   FileReader fileReader = new FileReader(document);
		   //Wrap FileReader in BufferReader
		   BufferedReader bufferedReader = new BufferedReader(fileReader);
		   
		   //Writing to new file
		   FileWriter fileWriter = new FileWriter(filenameForTag);
		   PrintWriter printWriter = new PrintWriter(fileWriter);
		   
		   
		   while((line = bufferedReader.readLine()) != null) {
			   StringTokenizer st1 = new StringTokenizer(line, ",// //.//'//://-//\t", true);

			   while (st1.hasMoreTokens()) {
				   //Assign current token to temp String variable
				   String temp = st1.nextToken();//focus on current token
				   
				   //Find index of matirx where word exists
				   int indexFound = wordCheck(temp, matrix);
				   //If found indexFound would be > -1
				   if(indexFound != -1) {
					   //System.out.print(temp+"/"+matrix[1][indexFound]);
					   printWriter.print(temp+"/"+matrix[1][indexFound]);
				   }
				   //Else it is just regular
				   else{
					   //System.out.print(temp);
					   printWriter.print(temp);
				   }

				   
			   }
			   //System.out.println(" ");
			   printWriter.println(" ");
		   }
		   bufferedReader.close();
		   printWriter.close();
	   }
	   catch(FileNotFoundException ex){
		   System.out.println("File: "+document+" not found");
		   
	   }
	   catch(IOException ex) {
		   System.out.println("Error opening "+document);
		   
	   }
   
   }
}